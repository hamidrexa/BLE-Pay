
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "nordic_common.h"
#include "nrf.h"
#include "app_error.h"
#include "ble.h"
#include "ble_hci.h"
#include "ble_srv_common.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "ble_conn_params.h"
#include "boards.h"
#include "softdevice_handler.h"
#include "app_timer.h"
#include "device_manager.h"
#include "pstorage.h"
#include "app_trace.h"
#include "bsp.h"
#include "bsp_btn_ble.h"
#include "sensorsim.h"
#include "nrf_gpio.h"
#include "ble_hci.h"
#include "ble_advdata.h"
#include "ble_advertising.h"
#include "nrf_delay.h"
#include "my_service.h"
#include "SEGGER_RTT.h"
#include "app_pwm.h"
#include "my_pstorage.h"
#include "nrf_drv_adc.h"

#define BUTTON_PIN			26
#define BUZZER_PIN 			27
#define LED1_G 					4 
#define LED1_R 					5 
#define CHARG_MEASURE_PIN_IN_ADC	NRF_ADC_CONFIG_INPUT_2
#define CHARG_CONTROL		30
#define CHARG_MEASURE_PIN_IN			29
#define CHARG_MEASURE_PIN_OUT			28

#define TEN_MS_INTERVAL									APP_TIMER_TICKS(10, APP_TIMER_PRESCALER)
#define ONE_HUNDRED_MS_INTERVAL					APP_TIMER_TICKS(100, APP_TIMER_PRESCALER)

// battery levels
#define LOW_BATTERY_LEVEL		512 // 3.6 v
#define FULL_BATTERY_LEVEL	597 // 4.19 v

#define IS_SRVC_CHANGED_CHARACT_PRESENT  1                                          /**< Include or not the service_changed characteristic. if not enabled, the server's database cannot be changed for the lifetime of the device*/

#define CENTRAL_LINK_COUNT               0                                          /**< Number of central links used by the application. When changing this number remember to adjust the RAM settings*/
#define PERIPHERAL_LINK_COUNT            1                                          /**< Number of peripheral links used by the application. When changing this number remember to adjust the RAM settings*/

#define DEVICE_NAME                      "PHONE_PAY_1"                          /**< Name of device. Will be included in the advertising data. */
#define MANUFACTURER_NAME                "NordicSemiconductor"                      /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                 100                                        /**< The advertising interval (in units of 0.625 ms. This value corresponds to 25 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0 //180                                    /**< The advertising timeout in units of seconds. */

#define APP_TIMER_PRESCALER              0                                          /**< Value of the RTC1 PRESCALER register. */
#define APP_TIMER_OP_QUEUE_SIZE          4                                          /**< Size of timer operation queues. */

#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(8, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.025 seconds). */
#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(12, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.040 second). */
#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY   APP_TIMER_TICKS(5000, APP_TIMER_PRESCALER) /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY    APP_TIMER_TICKS(30000, APP_TIMER_PRESCALER)/**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SEC_PARAM_BOND                   1                                          /**< Perform bonding. */
#define SEC_PARAM_MITM                   0                                          /**< Man In The Middle protection not required. */
#define SEC_PARAM_LESC                   0                                          /**< LE Secure Connections not enabled. */
#define SEC_PARAM_KEYPRESS               0                                          /**< Keypress notifications not enabled. */
#define SEC_PARAM_IO_CAPABILITIES        BLE_GAP_IO_CAPS_NONE                       /**< No I/O capabilities. */
#define SEC_PARAM_OOB                    0                                          /**< Out Of Band data not available. */
#define SEC_PARAM_MIN_KEY_SIZE           7                                          /**< Minimum encryption key size. */
#define SEC_PARAM_MAX_KEY_SIZE           16                                         /**< Maximum encryption key size. */

#define DEAD_BEEF                        0xDEADBEEF                                 /**< Value used as error code on stack dump, can be used to identify stack location on stack unwind. */

static dm_application_instance_t         m_app_handle;                              /**< Application identifier allocated by device manager */

static uint16_t                          m_conn_handle = BLE_CONN_HANDLE_INVALID;   /**< Handle of the current connection. */

/*---------------- ADC ----------------*/
#define ADC_BUFFER_SIZE 4
static nrf_adc_value_t       adc_buffer[ADC_BUFFER_SIZE]; 
static nrf_drv_adc_channel_t m_channel_config = NRF_DRV_ADC_DEFAULT_CHANNEL(CHARG_MEASURE_PIN_IN_ADC); 
/*-------------- END ADC --------------*/

uint16_t battery_level=0;

APP_PWM_INSTANCE(PWM1,2);

struct UX_strategy {
	bool flag;
	uint8_t count;
};
static struct UX_strategy buzzer = {false,0};
static struct UX_strategy one_buzz = {false,0};
static struct UX_strategy two_buzz = {false,0};
static struct UX_strategy turn_off_buzz = {false,0};
static struct UX_strategy turn_on_buzz = {false,0};
static struct UX_strategy full_charged = {false,0};
static struct UX_strategy charging = {false,0};
static struct UX_strategy low_battery = {false,0};
static struct UX_strategy BLE_connected = {false,0};
static struct UX_strategy BLE_disconnected_white_list_adv = {false,0};
static struct UX_strategy BLE_disconnected_public_adv = {false,0};

uint16_t PWM_period = 420,read_battery_count = 0;

// YOUR_JOB: Use UUIDs for service(s) used in your application.
static ble_uuid_t m_adv_uuids[] = {{BLE_UUID_DEVICE_INFORMATION_SERVICE, BLE_UUID_TYPE_BLE}}; /**< Universally unique service identifiers. */

                                   
/**@brief Callback function for asserts in the SoftDevice.
 *
 * @details This function will be called in case of an assert in the SoftDevice.
 *
 * @warning This handler is an example only and does not fit a final product. You need to analyze
 *          how your product is supposed to react in case of Assert.
 * @warning On assert from the SoftDevice, the system can only recover on reset.
 *
 * @param[in] line_num   Line number of the failing ASSERT call.
 * @param[in] file_name  File name of the failing ASSERT call.
 */
void assert_nrf_callback(uint16_t line_num, const uint8_t * p_file_name)
{
    app_error_handler(DEAD_BEEF, line_num, p_file_name);
}



// HARD CODE:
// my_service
// FROM_SERVICE_TUTORIAL: Declare a service structure for our application
ble_os_t my_service;

bool dfu_flag=false,button_state=false,read_battery_flag=false,low_battery_flag=false;
APP_TIMER_DEF(timer_10_ms_id);
APP_TIMER_DEF(timer_100_ms_id);

// ************************************
// HARD CODE:
// I2C
#include "i2c.h"		
		// I2C PINs: Defined in "i2c.h"
		
// ***********************************
// HARD CODE:
// RNG
#ifdef RNG
		#include "my_RNG.h"
#endif

// ***********************************
// HARD CODE:
// AES
#ifdef AES
		#include "TI_aes.h"
#endif

uint8_t button_value=0,button_press_time=0,button_unpress_time=0;
static void timer_10_ms_timeout_handler(void * p_context)
{
	if(nrf_gpio_pin_read(BUTTON_PIN))
	{
		if(button_value>0)button_value--;
		if(button_value==0)button_state = false;
	}else
	{
		if(button_value<200)button_value++;
		if(button_value==200)button_state = true;		
	}
}			

static void adc_event_handler(nrf_drv_adc_evt_t const * p_event)
{
	if (p_event->type == NRF_DRV_ADC_EVT_DONE)
	{
		uint32_t i;
		for (i = 0; i < p_event->data.done.size; i++)
		{
		}
	}
}

void LEDs_off(struct UX_strategy* strategy)
{
	if(((strategy == &charging) || (strategy == &low_battery)) && (!low_battery.flag) && (!charging.flag))
	{
		nrf_gpio_pin_set(LED1_R);				
	}else if(((strategy == &BLE_disconnected_white_list_adv) || (strategy == &BLE_disconnected_public_adv)) && (!BLE_disconnected_white_list_adv.flag) && (!BLE_disconnected_public_adv.flag))
	{
		nrf_gpio_pin_set(LED1_G);
	}else if(((strategy == &full_charged) || (strategy == &BLE_connected)) && (!full_charged.flag) && (!BLE_connected.flag))
	{
//		nrf_gpio_pin_set(LED1_G);
	}
}

void buzzer_off(void)
{
	while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
}

void strategy_state(struct UX_strategy* strategy,bool state)
{
	if(state)
	{
		strategy->flag=1;
		strategy->count=0;
	}else
	{
		strategy->flag=0;
		strategy->count=0;
		if(strategy == &buzzer || strategy == &one_buzz || strategy == &two_buzz || strategy == &turn_off_buzz || strategy == &turn_on_buzz)
		{
			buzzer_off();
		}else
		{
			LEDs_off(strategy);
		}
	}
}

static void adc_config(void)
{
	ret_code_t ret_code;
	nrf_drv_adc_config_t config = NRF_DRV_ADC_DEFAULT_CONFIG;
	m_channel_config.config.config.resolution = NRF_ADC_CONFIG_RES_10BIT;
	m_channel_config.config.config.input = NRF_ADC_CONFIG_SCALING_INPUT_ONE_THIRD;
	m_channel_config.config.config.reference = NRF_ADC_CONFIG_REF_VBG;			

	ret_code = nrf_drv_adc_init(&config, adc_event_handler);
	APP_ERROR_CHECK(ret_code);

	nrf_drv_adc_channel_enable(&m_channel_config);
}

void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    //pwm_ready_flag = true;
}

void reinit_PWM(uint16_t period)
{
	int32_t	err_code;	
	app_pwm_uninit(&PWM1);
	app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(period, BUZZER_PIN);
	pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
	pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;			
	err_code = app_pwm_init(&PWM1,&pwm1_cfg,pwm_ready_callback);
	APP_ERROR_CHECK(err_code);
	app_pwm_enable(&PWM1);
}

void init_PWM(uint16_t period)
{
	int32_t	err_code;		
	app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(period, BUZZER_PIN);
	pwm1_cfg.pin_polarity[0] = APP_PWM_POLARITY_ACTIVE_HIGH;
	pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;			
	err_code = app_pwm_init(&PWM1,&pwm1_cfg,pwm_ready_callback);
	APP_ERROR_CHECK(err_code);
	app_pwm_enable(&PWM1);
}

static void timer_100_ms_timeout_handler(void * p_context)	
{
	read_battery_count++;
	if(read_battery_count == 300)
	{
		read_battery_count = 0;
		read_battery_flag = true;
	}
	//------------------- buzzer strategy management-----------------	
	if(buzzer.flag)
	{
		if(buzzer.count==0)
		{			
			reinit_PWM(420);	
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
		}else if(buzzer.count==2)
		{
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
		}else if(buzzer.count==1 || buzzer.count==3)
		{
			while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
		}
		(buzzer.count>8)?(buzzer.count=0):(buzzer.count++);
	}else if(one_buzz.flag)
	{
		if(one_buzz.count==0)
		{
			reinit_PWM(420);
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
			one_buzz.count++;
		}else if(one_buzz.count==1)
		{
			strategy_state(&one_buzz,false);
		}		
	}else if(two_buzz.flag)
	{
		if(two_buzz.count==0)
		{
			reinit_PWM(420);			
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
			two_buzz.count++;
		}else if(two_buzz.count==2)
		{
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);
			two_buzz.count++;
		}else if(two_buzz.count==1 )
		{
			while (app_pwm_channel_duty_set(&PWM1, 0, 0) == NRF_ERROR_BUSY);
			two_buzz.count++;
		}else if(two_buzz.count==3 )
		{
			strategy_state(&two_buzz,false);
		}		
	}else if(turn_off_buzz.flag)
	{
		if(turn_off_buzz.count==0 || turn_off_buzz.count==1 || turn_off_buzz.count==2)
		{
			if(turn_off_buzz.count==0)
			{
				PWM_period = 700;
			}else if(turn_off_buzz.count==1)
			{
				PWM_period = 900;
			}else
			{
				PWM_period = 1200;
			}		

			reinit_PWM(PWM_period);
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);				
			turn_off_buzz.count++;
		}else if(turn_off_buzz.count==3)
		{
			strategy_state(&turn_off_buzz,false);
		}		
	}else if(turn_on_buzz.flag)
	{
		if(turn_on_buzz.count==0 || turn_on_buzz.count==1 || turn_on_buzz.count==2)
		{												
			if(turn_on_buzz.count==0)
			{
				PWM_period = 1200;
			}else if(turn_on_buzz.count==1)
			{
				PWM_period = 900;
			}else
			{
				PWM_period = 700;
			}
			
			reinit_PWM(PWM_period);
			while (app_pwm_channel_duty_set(&PWM1, 0, 50) == NRF_ERROR_BUSY);				
			turn_on_buzz.count++;
		}else if(turn_on_buzz.count==3)
		{
			strategy_state(&turn_on_buzz,false);
		}		
	}
	//----------------- end buzzer strategy section----------------	
	
	//------------------- led strategy management-----------------
	if(low_battery.flag)
	{
		if(low_battery.count==0)
		{			
			nrf_gpio_pin_clear(LED1_R);
		}else if(low_battery.count==1)
		{			
			nrf_gpio_pin_set(LED1_R);
			nrf_gpio_pin_set(LED1_G);
		}
		(low_battery.count>18)?(low_battery.count=0):(low_battery.count++);
	}
	//----------------- end led strategy section----------------
}

// ***********************************
// HARD CODE:
// HardTech_Timeout_Detector
#ifdef HardTech_Timeout_Detector  
		
		static bool			Connect_Event_Flag	= false;
		static uint32_t	timeout_Counter			= 0;
		
		APP_TIMER_DEF(m_HardTech_Timeout_Detector_timer_id); 
		#define TIMER_INTERVAL_HardTech_Timeout_Detector	APP_TIMER_TICKS(100, APP_TIMER_PRESCALER) // Defines the interval between consecutive app timer interrupts in milliseconds
//		static uint32_t timeout_varible = 5000; // 5000ms = 5s

		static void timer_timeout_handler_HardTech_Timeout_Detector(void * p_context)
		{
				if(Connect_Event_Flag) {
						timeout_Counter++;
						if(timeout_Counter > 100) {
								LOG_String("Connection Timeout !");
								sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
								timeout_Counter = 0;
						}
				} else {
						timeout_Counter = 0;
				}					
		}
#endif
		
// ***********************************
// HARD CODE:
// GPIOs
#ifdef GPIOs

		APP_TIMER_DEF(m_GPIOs_timer_id); 
		#define TIMER_INTERVAL_GPIOs	APP_TIMER_TICKS(100, APP_TIMER_PRESCALER) // Defines the interval between consecutive app timer interrupts in milliseconds
		
static void timer_timeout_handler_GPIOs(void * p_context)
{
				uint8_t				char_1[20];			
	
				//Get Characteristic Value:
				ble_gatts_value_t gatts_value;
				gatts_value.len 			= 10; 						// number_of_bytes_you_want_to_read
				gatts_value.offset  	= 0;							// location_for_my_value
				gatts_value.p_value 	= char_1;
				sd_ble_gatts_value_get(	(&my_service) -> conn_handle, 									//uint16_t conn_handle
																(&my_service) -> char_handles[0].value_handle, 	//uint16_t handle
																(&gatts_value) 																	//ble_gatts_value_t *p_value
															);
}		
#endif

/**@brief Function for the Timer initialization.
 *
 * @details Initializes the timer module. This creates and starts application timers.
 */
static void timers_init(void)
{
    // Initialize timer module.
    APP_TIMER_INIT(APP_TIMER_PRESCALER, APP_TIMER_OP_QUEUE_SIZE, false);

    uint32_t err_code;
		
		// HARD CODE:
		// HardTech_Timeout_Detector
		#ifdef HardTech_Timeout_Detector 
				err_code = app_timer_create(&m_HardTech_Timeout_Detector_timer_id, APP_TIMER_MODE_REPEATED, timer_timeout_handler_HardTech_Timeout_Detector);
				APP_ERROR_CHECK(err_code);
		#endif
		
		// HARD CODE:
		// GPIOs
		#ifdef GPIOs
				err_code = app_timer_create(&m_GPIOs_timer_id, APP_TIMER_MODE_REPEATED, timer_timeout_handler_GPIOs);
				APP_ERROR_CHECK(err_code);		
		#endif
		
		err_code = app_timer_create(&timer_10_ms_id, APP_TIMER_MODE_REPEATED, timer_10_ms_timeout_handler);
		APP_ERROR_CHECK(err_code);		

		err_code = app_timer_create(&timer_100_ms_id, APP_TIMER_MODE_REPEATED, timer_100_ms_timeout_handler);
		APP_ERROR_CHECK(err_code);	
}

// HARD CODE:
/**@brief Function for starting timers.
*/
static void application_timers_start(void)
{
    uint32_t err_code;

		// HARD CODE:
		// HardTech_Timeout_Detector
		#ifdef HardTech_Timeout_Detector
				err_code = app_timer_start(m_HardTech_Timeout_Detector_timer_id, TIMER_INTERVAL_HardTech_Timeout_Detector, NULL);
				APP_ERROR_CHECK(err_code);
		#endif

		// HARD CODE:
		// GPIOs
		#ifdef GPIOs
				err_code = app_timer_start(m_GPIOs_timer_id, TIMER_INTERVAL_GPIOs, NULL);
				APP_ERROR_CHECK(err_code);
		#endif

		err_code = app_timer_start(timer_10_ms_id, TEN_MS_INTERVAL, NULL);
		APP_ERROR_CHECK(err_code);		
	
		err_code = app_timer_start(timer_100_ms_id, ONE_HUNDRED_MS_INTERVAL, NULL);
		APP_ERROR_CHECK(err_code);			
}


uint8_t	BLEaddress[16] = {0x11, 0x22, 0x33, 0x44, 
													0x55, 0x66, 0x77, 0x88,
													0x99, 0xAA, 0xBB, 0xCC,
													0xDD, 0xEE, 0xFF, 0x11};


/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    uint32_t                err_code;
	
    // Declearing parameter structs. Try to go to the struct definitions to get
	  // more information about what parameters they contain	
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    // A simple macro that sets the Security Mode and Level bits in sec_mode
	  // to require no protection (open link)	
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    // Store the device name and security mode in the SoftDevice. Our name is defined to "HelloWorld" in the beginning of this file
    err_code = sd_ble_gap_device_name_set(&sec_mode,
                                          (const uint8_t *)DEVICE_NAME,
                                          strlen(DEVICE_NAME));
																					
    APP_ERROR_CHECK(err_code); // Check for errors

    // Always initialize all fields in structs to zero or you might get unexpected behaviour
    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    // Populate the GAP connection parameter struct
    gap_conn_params.min_conn_interval = MIN_CONN_INTERVAL;
    gap_conn_params.max_conn_interval = MAX_CONN_INTERVAL;
    gap_conn_params.slave_latency     = SLAVE_LATENCY;
    gap_conn_params.conn_sup_timeout  = CONN_SUP_TIMEOUT;

    // Set GAP Peripheral Preferred Connection Parameters
    // The device use these prefered values when negotiating connection terms with another device
    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
    APP_ERROR_CHECK(err_code);

		// HARD CODE:
		// "Public address"							:	a global, fixed address. It must be registered with 
		//																the IEEE Registration Authority																			
		// "Random Static address" 			:	a random number that can either be generated 
		//																every time the device boots up or can stay the 
		//																same for the lifetime of the device.																			
		// "Private Resolvable address" :	generated from an identity resolving key (IRK) 
		//																and a random number, and they can be changed 
		//																often (even during the lifetime of a connection)																			
																					
		// HARD CODE:
		// The default address type in this example is �Random static address�																					
																					
		// HARD CODE:
		// Tx_POWER																					
		sd_ble_gap_tx_power_set(4);
		
		// HARD CODE:
    // Set Appearence										  
		sd_ble_gap_appearance_set(BLE_APPEARANCE_HID_MOUSE);
		APP_ERROR_CHECK(err_code);// Check for errors                                 

		// HARD CODE:
    // This will set the address type to �Private Resolvable address�:
		ble_gap_addr_t gap_address;
		gap_address.addr_type = BLE_GAP_ADDR_TYPE_RANDOM_PRIVATE_RESOLVABLE;
		
		uint8_t my_address[BLE_GAP_ADDR_LEN] = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66};		
		
		for(int i=0; i<BLE_GAP_ADDR_LEN; i++) {
				my_address[i] = BLEaddress[i];
		}
		
		for(int i=0; i<BLE_GAP_ADDR_LEN; i++){
				gap_address.addr[i] = my_address[i];
		}
		//err_code = sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_AUTO, &gap_address);
		err_code = sd_ble_gap_address_set(BLE_GAP_ADDR_CYCLE_MODE_NONE, &gap_address);
		APP_ERROR_CHECK(err_code);// Check for errors
}


/**@brief Function for initializing services that will be used by the application.
 */
static void services_init(void)
{
		// HARD CODE:
		// my_service
		my_service_init(&my_service);
	
}


/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
    if (p_evt->evt_type == BLE_CONN_PARAMS_EVT_FAILED)
    {
        APP_ERROR_CHECK( sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE) );
    }
}


/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
    APP_ERROR_HANDLER(nrf_error);
}


/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
    uint32_t               err_code;
    ble_conn_params_init_t cp_init;

    memset(&cp_init, 0, sizeof(cp_init));

    cp_init.p_conn_params                  = NULL;
    cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
    cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
    cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
    cp_init.start_on_notify_cccd_handle    = BLE_GATT_HANDLE_INVALID;
    cp_init.disconnect_on_fail             = false;
    cp_init.evt_handler                    = on_conn_params_evt;
    cp_init.error_handler                  = conn_params_error_handler;

    err_code = ble_conn_params_init(&cp_init);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for putting the chip into sleep mode.
 *
 * @note This function will not return.
 */
static void sleep_mode_enter(void)
{
    uint32_t err_code = bsp_indication_set(BSP_INDICATE_IDLE);
    APP_ERROR_CHECK(err_code);

    // Prepare wakeup buttons.
    err_code = bsp_btn_ble_sleep_mode_prepare();
    APP_ERROR_CHECK(err_code);

    // Go to system-off mode (this function will not return; wakeup will cause a reset).
    err_code = sd_power_system_off();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    uint32_t err_code;

    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            err_code = bsp_indication_set(BSP_INDICATE_ADVERTISING);
            APP_ERROR_CHECK(err_code);
            break;
        case BLE_ADV_EVT_IDLE:
            sleep_mode_enter();
            break;
        default:
            break;
    }
}
/*					 *
 *					 *
 * HARD CODE *
 *					 *
 *					 *
 */
#ifdef Phase_3
		#define	State_Wait_until_Notify_be_enabled	0
		#define	State_Send_OK_plus_Rand							1
		#define	State_Rand_plus_OK_Received					2
		#define	State_Successful_Received						3
		
		static int State = State_Wait_until_Notify_be_enabled;
		static int Counter_Data_Available = 0;
		uint8_t PrivateKey[SOC_ECB_KEY_LENGTH] = {	0x12, 0x34, 0x12, 0x34,
																								0x12, 0x34, 0x12, 0x34, 
																								0x12, 0x34, 0x12, 0x34,
																								0x12, 0x34, 0x12, 0x34	};

		void State_Assigner() {
										if(State == State_Wait_until_Notify_be_enabled) {
												State = State_Send_OK_plus_Rand;
												return;
										}

										if(State == State_Send_OK_plus_Rand) {
												State = State_Rand_plus_OK_Received;
												return;
										}

										if(State == State_Rand_plus_OK_Received) {
												State = State_Successful_Received;
												return;
										}										
		}
#endif //Phase_3

#ifdef Phase_3
		uint8_t OK_plus_Rand[SOC_ECB_CLEARTEXT_LENGTH]	= {	0x50, 0x68, 0x6f, 0x6e,
																												0x65, 0x70, 0x61, 0x79,
																												0x00, 0x00, 0x00, 0x00,
																												0x00, 0x00, 0x00, 0x00	};

		void	Func_Send_OK_plus_Rand() {
				//RNG:
				uint8_t p_buff[RANDOM_BUFF_SIZE];
				uint8_t length = random_vector_generate(p_buff,RANDOM_BUFF_SIZE);
				//LOG_uint8_t(length);
				for(int i=0; i<length; i++) {
						OK_plus_Rand[i+8] = p_buff[i];
						//LOG_uint8_t(OK_plus_Rand[i+8]);
				}
				
				uint8_t Local_OK_plus_Rand[SOC_ECB_CLEARTEXT_LENGTH];
				for(int i=0; i<SOC_ECB_CLEARTEXT_LENGTH; i++) {
						Local_OK_plus_Rand[i] = OK_plus_Rand[i];
						LOG_String_and_uint8_t("OK_plus_Rand[] = ", Local_OK_plus_Rand[i]);
				}

				unsigned char * encrypted_OK_plus_Rand = (unsigned char*) Local_OK_plus_Rand;
				unsigned char * key   								 = (unsigned char*) PrivateKey;
				aes_encrypt(encrypted_OK_plus_Rand, key);

				//send encrypted_OK_plus_Rand
				uint8_t char_1[SOC_ECB_CIPHERTEXT_LENGTH];
				for(int i=0; i<SOC_ECB_CIPHERTEXT_LENGTH; i++) {
						char_1[i] = ( (uint8_t)(encrypted_OK_plus_Rand[i]) );
				}
				
				nrf_delay_ms(50);
				uint8_t 	char_length 					 = SOC_ECB_CIPHERTEXT_LENGTH;			
				uint32_t 	index_of_characeristic = 1;		
				my_characteristic_update(&my_service, char_length, char_1, index_of_characeristic); //updating characteristic value (char_2)
		}
#endif //Phase_3		

#ifdef Phase_3
		bool	Func_Check_Rand_plus_OK() {
				//Get Characteristic Value:
				uint8_t char_1[16];
				ble_gatts_value_t gatts_value;
				gatts_value.len 			= 16; 								// number_of_bytes_you_want_to_read
				gatts_value.offset  	=  0;									// location_for_my_value
				gatts_value.p_value 	= (uint8_t*)char_1;		//
				sd_ble_gatts_value_get(	(&my_service) -> conn_handle, 									//uint16_t conn_handle
																(&my_service) -> char_handles[0].value_handle, 	//uint16_t handle
																(&gatts_value) 																	//ble_gatts_value_t *p_value
															);
							
				unsigned char * state = (unsigned char*) char_1;
				unsigned char * key   = (unsigned char*) PrivateKey;

				aes_decrypt(state,key);

				uint8_t Expected_Rand_plus_OK[SOC_ECB_CLEARTEXT_LENGTH];
				for(int i=0; i<8; i++) {
						Expected_Rand_plus_OK[i]	 = OK_plus_Rand[i+8];
						Expected_Rand_plus_OK[i+8] = OK_plus_Rand[i];
				}

				uint8_t Received_Rand_plus_OK[SOC_ECB_CIPHERTEXT_LENGTH];
				uint8_t Errors_In_Rand_plus_OK = 0;
				for(int i=0; i<SOC_ECB_CIPHERTEXT_LENGTH; i++) 
				{
						Received_Rand_plus_OK[i] = ( (uint8_t)(state[i]) );
						//LOG("Received_Rand_plus_OK[] =", Received_Rand_plus_OK[i]);
						if(Received_Rand_plus_OK[i] != Expected_Rand_plus_OK[i]) {
								Errors_In_Rand_plus_OK++;
						}
				}
				LOG("Errors_In_Rand_plus_OK =", Errors_In_Rand_plus_OK);
				
				if(Errors_In_Rand_plus_OK)
						return false;
				else
						return true;
		}
#endif //Phase_3
				
		
#ifdef Phase_3
		bool	Func_Check_Successful() {
				//Get Characteristic Value:
				uint8_t char_1[10];
				ble_gatts_value_t gatts_value;
				gatts_value.len 			= 10; 								// number_of_bytes_you_want_to_read
				gatts_value.offset  	=  0;									// location_for_my_value
				gatts_value.p_value 	= (uint8_t*)char_1;		//
				sd_ble_gatts_value_get(	(&my_service) -> conn_handle, 									//uint16_t conn_handle
																(&my_service) -> char_handles[0].value_handle, 	//uint16_t handle
																(&gatts_value) 																	//ble_gatts_value_t *p_value
															);

				uint8_t Expected_Successful[10] = {	0x53, 0x75, 0x63, 0x63,
																						0x65, 0x73, 0x73, 0x66, 
																						0x75, 0x6c							};

				uint8_t Received_Successful[10];
				uint8_t Errors_In_Successful = 0;
				for(int i=0; i<10; i++) 
				{
						Received_Successful[i] = char_1[i];
						//LOG("Received_Successful[] =", Received_Successful[i]);
						if(Received_Successful[i] != Expected_Successful[i]) {
								Errors_In_Successful++;
						}
				}
				LOG("Errors_In_Successful =", Errors_In_Successful);
				
				if(Errors_In_Successful)
						return false;
				else
						return true;
		}
#endif //Phase_3
		
#ifdef Phase_3
		void	Func_Send_Payment_Done() {
				
				uint8_t char_1[11]	=	{	0x70, 0x61, 0x79, 0x6d,
																0x65, 0x6e, 0x74, 0x44,
																0x6f, 0x6e, 0x65				};

				uint8_t 	char_length 					 = 11;			
				uint32_t 	index_of_characeristic =  1;		
				my_characteristic_update(&my_service, char_length, char_1, index_of_characeristic); //updating characteristic value (char_2)
		}
#endif //Phase_3

		
static bool 		Write_Event_Flag 		= false;
		
/**@brief Function for handling the Application's BLE Stack events.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void on_ble_evt(ble_evt_t * p_ble_evt)
{
    uint32_t err_code;
		
		static bool 		flag_GPIOs_timer_has_been_stopped = 0;
	
    switch (p_ble_evt->header.evt_id)
            {
				case BLE_GATTS_EVT_WRITE:
						// HARD CODE
						LOG_String("Write Event");
				
						Write_Event_Flag = true;
												
						#ifdef AES
								Counter_Data_Available++;
						#endif //AES																									
																																		
				break;
				
        case BLE_GAP_EVT_CONNECTED:
            err_code = bsp_indication_set(BSP_INDICATE_CONNECTED);
            APP_ERROR_CHECK(err_code);
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
				
						// HARD CODE
						LOG_String("Connect Event");

						Connect_Event_Flag = true;			
															
						break;

        case BLE_GAP_EVT_DISCONNECTED:
            m_conn_handle = BLE_CONN_HANDLE_INVALID;
				
						// HARD CODE
						LOG_String("Disconnect Event");

						Connect_Event_Flag = false;
				
						// HARD CODE:
						#ifdef AES
								State = State_Wait_until_Notify_be_enabled;
						#endif //AES
				
						// HARD CODE:
						#ifdef Phase_3
								OK_plus_Rand[0]	= 0x50;
								OK_plus_Rand[1]	= 0x68;
								OK_plus_Rand[2]	= 0x6f;
								OK_plus_Rand[3]	= 0x6e;
								OK_plus_Rand[4]	= 0x65; 
								OK_plus_Rand[5]	= 0x70; 
								OK_plus_Rand[6]	= 0x61; 
								OK_plus_Rand[7]	= 0x79;
						#endif //Phase_3					
												
            break;

        default:
            // No implementation needed.
            break;
    }
}


/**@brief Function for dispatching a BLE stack event to all modules with a BLE stack event handler.
 *
 * @details This function is called from the BLE Stack event interrupt handler after a BLE stack
 *          event has been received.
 *
 * @param[in] p_ble_evt  Bluetooth stack event.
 */
static void ble_evt_dispatch(ble_evt_t * p_ble_evt)
{
    dm_ble_evt_handler(p_ble_evt);
    ble_conn_params_on_ble_evt(p_ble_evt);
    bsp_btn_ble_on_ble_evt(p_ble_evt);
    on_ble_evt(p_ble_evt);
    ble_advertising_on_ble_evt(p_ble_evt);
	
    // Step 3.C, Call ble_my_service_on_ble_evt() to do housekeeping of ble connections related to our service and characteristic
		ble_my_service_on_ble_evt(&my_service, p_ble_evt);

}


/**@brief Function for dispatching a system event to interested modules.
 *
 * @details This function is called from the System event interrupt handler after a system
 *          event has been received.
 *
 * @param[in] sys_evt  System stack event.
 */
static void sys_evt_dispatch(uint32_t sys_evt)
{
    pstorage_sys_event_handler(sys_evt);
    ble_advertising_on_sys_evt(sys_evt);
}


/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
			uint32_t err_code;

			nrf_clock_lf_cfg_t clock_lf_cfg = {.source        = NRF_CLOCK_LF_SRC_RC,            
																				 .rc_ctiv       = 1,                                
																				 .rc_temp_ctiv  = 1,                                
																				 .xtal_accuracy = NRF_CLOCK_LF_XTAL_ACCURACY_250_PPM};

																				 
    // Initialize the SoftDevice handler module.
    SOFTDEVICE_HANDLER_INIT(&clock_lf_cfg, NULL);
    
    ble_enable_params_t ble_enable_params;
    err_code = softdevice_enable_get_default_config(CENTRAL_LINK_COUNT,
                                                    PERIPHERAL_LINK_COUNT,
                                                    &ble_enable_params);
    APP_ERROR_CHECK(err_code);
    
    //Check the ram settings against the used number of links
    CHECK_RAM_START_ADDR(CENTRAL_LINK_COUNT,PERIPHERAL_LINK_COUNT);
    
    // Enable BLE stack.
    err_code = softdevice_enable(&ble_enable_params);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_ble_evt_handler_set(ble_evt_dispatch);
    APP_ERROR_CHECK(err_code);

    // Register with the SoftDevice handler module for BLE events.
    err_code = softdevice_sys_evt_handler_set(sys_evt_dispatch);
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for handling events from the BSP module.
 *
 * @param[in]   event   Event generated by button press.
 */
void bsp_event_handler(bsp_event_t event)
{
    uint32_t err_code;
    switch (event)
    {
        case BSP_EVENT_SLEEP:
            sleep_mode_enter();
            break;

        case BSP_EVENT_DISCONNECT:
            err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        case BSP_EVENT_WHITELIST_OFF:
            err_code = ble_advertising_restart_without_whitelist();
            if (err_code != NRF_ERROR_INVALID_STATE)
            {
                APP_ERROR_CHECK(err_code);
            }
            break;

        default:
            break;
    }
}


/**@brief Function for handling the Device Manager events.
 *
 * @param[in] p_evt  Data associated to the device manager event.
 */
static uint32_t device_manager_evt_handler(dm_handle_t const * p_handle,
                                           dm_event_t const  * p_event,
                                           ret_code_t        event_result)
{
    APP_ERROR_CHECK(event_result);

#ifdef BLE_DFU_APP_SUPPORT
    if (p_event->event_id == DM_EVT_LINK_SECURED)
    {
        app_context_load(p_handle);
    }
#endif // BLE_DFU_APP_SUPPORT

    return NRF_SUCCESS;
}


/**@brief Function for the Device Manager initialization.
 *
 * @param[in] erase_bonds  Indicates whether bonding information should be cleared from
 *                         persistent storage during initialization of the Device Manager.
 */
static void device_manager_init(bool erase_bonds)
{
    uint32_t               err_code;
    dm_init_param_t        init_param = {.clear_persistent_data = erase_bonds};
    dm_application_param_t register_param;

    // Initialize persistent storage module.
    err_code = pstorage_init();
    APP_ERROR_CHECK(err_code);

    err_code = dm_init(&init_param);
    APP_ERROR_CHECK(err_code);

    memset(&register_param.sec_param, 0, sizeof(ble_gap_sec_params_t));

    register_param.sec_param.bond         = SEC_PARAM_BOND;
    register_param.sec_param.mitm         = SEC_PARAM_MITM;
    register_param.sec_param.lesc         = SEC_PARAM_LESC;
    register_param.sec_param.keypress     = SEC_PARAM_KEYPRESS;
    register_param.sec_param.io_caps      = SEC_PARAM_IO_CAPABILITIES;
    register_param.sec_param.oob          = SEC_PARAM_OOB;
    register_param.sec_param.min_key_size = SEC_PARAM_MIN_KEY_SIZE;
    register_param.sec_param.max_key_size = SEC_PARAM_MAX_KEY_SIZE;
    register_param.evt_handler            = device_manager_evt_handler;
    register_param.service_type           = DM_PROTOCOL_CNTXT_GATT_SRVR_ID;

    err_code = dm_register(&m_app_handle, &register_param);
    APP_ERROR_CHECK(err_code);
		
}
		
#ifdef Phase_3
		static void advertising_init(void)
		{
				uint32_t      err_code;
				ble_advdata_t advdata;

				// Build advertising data struct to pass into @ref ble_advertising_init.
				memset(&advdata, 0, sizeof(advdata));

				advdata.name_type    		 				= BLE_ADVDATA_FULL_NAME;			
				advdata.include_appearance      = true;
				advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
				advdata.uuids_complete.uuid_cnt = sizeof(m_adv_uuids) / sizeof(m_adv_uuids[0]);
				advdata.uuids_complete.p_uuids  = m_adv_uuids;
			
				ble_adv_modes_config_t options = {0};
				options.ble_adv_fast_enabled  = BLE_ADV_FAST_ENABLED;
				options.ble_adv_fast_interval = APP_ADV_INTERVAL;
				options.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;
				
				// HARD CODE:
				// my_service
				err_code = ble_advertising_init(&advdata, NULL, &options, on_adv_evt, NULL);
				APP_ERROR_CHECK(err_code);
		}
#endif // Phase_3

	
/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}


/**@brief Function for application main entry.
 */
int main(void)
{
	uint32_t err_code;
	bool erase_bonds;

	// ***********************************
	// Initialize.
	timers_init();
	ble_stack_init();
	device_manager_init(erase_bonds);

	// ***********************************
	// HARD CODE:
	// PSTORAGE

	my_Pstorage_Registeration();

	LOG_String("Chip Start ...");

	uint8_t		operation;
	uint8_t		Destination;
	uint8_t*	InOut_Vector;

	//Load PrivateKey from FLASH (pstorage)
	operation			=	pstorage_operation_load;
	Destination		=	pstorage_Destination_PrivateKey;
	InOut_Vector	= PrivateKey;
	pstorage_load_store_clear_update(operation,
																	 Destination,
																	 InOut_Vector	//16 Bytes
	);
	for(int i=0; i<16; i++) {
			//LOG("PrivateKey[]", PrivateKey[i]);
	}

	//Load BLEaddress from FLASH (pstorage)		
	operation			=	pstorage_operation_load;
	Destination		=	pstorage_Destination_BLEaddress;
	InOut_Vector	= BLEaddress;
	pstorage_load_store_clear_update(operation,
																	 Destination,
																	 InOut_Vector	//16 Bytes
	);
	for(int i=0; i<16; i++) {
			//LOG("BLEaddress[]", BLEaddress[i]);
	}

	// Set BLEadress in first Programming
	if(BLEaddress[15]!=0x1a) {
			BLEaddress[0]=0x11;
			BLEaddress[1]=0x22;
			BLEaddress[2]=0x33;
			BLEaddress[3]=0x44;
			BLEaddress[4]=0x55;
			BLEaddress[5]=0x66;
			BLEaddress[15]=0x1a;
			operation			=	pstorage_operation_update;
			Destination		=	pstorage_Destination_BLEaddress;
			InOut_Vector	= BLEaddress;
			pstorage_load_store_clear_update(operation,
																			 Destination,
																			 InOut_Vector	//16 Bytes
			);
	}

	// ***********************************
	gap_params_init();
	advertising_init();
	services_init();
	conn_params_init();		

	// ***********************************
	// HARD CODE:
	// I2C
	int ret;
	ret = i2c_Init();

	// ***********************************
	// HARD CODE:
	// GPIOs
	#ifdef GPIOs
			nrf_gpio_cfg_output(LED1_G);
			nrf_gpio_cfg_output(LED1_R);
//			nrf_gpio_cfg_input(CHARG_MEASURE_PIN_OUT,GPIO_PIN_CNF_PULL_Disabled);
			nrf_gpio_cfg_output(CHARG_MEASURE_PIN_OUT);
			nrf_gpio_cfg_output(CHARG_CONTROL);
			nrf_gpio_cfg_input(BUTTON_PIN,NRF_GPIO_PIN_PULLUP);
			nrf_gpio_cfg_input(CHARG_MEASURE_PIN_IN,NRF_GPIO_PIN_PULLUP);
									
									
			nrf_gpio_pin_set(LED1_G);
			nrf_gpio_pin_set(LED1_R);
			nrf_gpio_pin_set(CHARG_MEASURE_PIN_OUT);
			nrf_gpio_pin_set(CHARG_CONTROL);
	#endif
	
	// ***********************************
	// HARD CODE:
	// RNG
	#ifdef RNG
			APP_ERROR_CHECK(nrf_drv_rng_init(NULL));
	#endif

	// ***********************************	
	// Start execution.
	application_timers_start();
	err_code = ble_advertising_start(BLE_ADV_MODE_FAST);
	APP_ERROR_CHECK(err_code);		

	adc_config();				
	nrf_drv_adc_buffer_convert(adc_buffer,ADC_BUFFER_SIZE);
	for (uint16_t i = 0; i < ADC_BUFFER_SIZE; i++)
	{
		// manually trigger ADC conversion
		nrf_drv_adc_sample();
		nrf_delay_us(1000);
	}
	battery_level  = ((adc_buffer[0]+adc_buffer[1]+adc_buffer[2]+adc_buffer[3])>>2);
	LOG_ADC(battery_level);
	if(battery_level<LOW_BATTERY_LEVEL)  // if battery level under 20%
	{
		strategy_state(&low_battery,true);			
		low_battery_flag=true;
	}else  // if battery level normal
	{
		strategy_state(&low_battery,false);					
		low_battery_flag=false;
	}	
	
	sd_power_gpregret_clr(0xff);	

	LOG_String("init completed");	

	init_PWM(420);			
	strategy_state(&turn_on_buzz,true);				

	static int Previous_Counter = 0;		
	// Enter main loop.
	for (;;)
	{
		power_manage();
	
		if(Write_Event_Flag) {
			
				Write_Event_Flag = false;
			
				//Check if PrivateKey should be changed:
			
				//Get Characteristic Value:
				uint8_t 					char_read[16];
				ble_gatts_value_t gatts_value;
				gatts_value.len 			= 16; 								// number_of_bytes_you_want_to_read
				gatts_value.offset  	= 0;									// location of char_3(PrivateKey)
				gatts_value.p_value 	= (uint8_t*)char_read;	
				sd_ble_gatts_value_get(	(&my_service) -> conn_handle, 									//uint16_t conn_handle
																(&my_service) -> char_handles[2].value_handle, 	//uint16_t handle
																(&gatts_value) 																	//ble_gatts_value_t *p_value
															);						
				uint8_t PrivateKey_Changed_Bytes = 0;
				for(int i=0; i<16; i++) {
						//LOG("char_read [2] =", char_read[i]);
						if(char_read[i] != 0x00) PrivateKey_Changed_Bytes++;
				}
				if(PrivateKey_Changed_Bytes) {
						uint8_t		operation			=	pstorage_operation_update;
						uint8_t		Destination		=	pstorage_Destination_PrivateKey;
						uint8_t*	InOut_Vector	= char_read;
						pstorage_load_store_clear_update(operation,
																						 Destination,
																						 InOut_Vector	//16 Bytes
						);
					
						sd_nvic_SystemReset();
				}
				
				//Check if BLEaddress should be changed:
			
				//Get Characteristic Value:
				gatts_value.len 			= 16; 								// number_of_bytes_you_want_to_read
				gatts_value.offset  	= 0;									// location of char_3(PrivateKey)
				gatts_value.p_value 	= (uint8_t*)char_read;	
				sd_ble_gatts_value_get(	(&my_service) -> conn_handle, 									//uint16_t conn_handle
																(&my_service) -> char_handles[3].value_handle, 	//uint16_t handle
																(&gatts_value) 																	//ble_gatts_value_t *p_value
															);
				uint8_t BLEaddress_Changed_Bytes = 0;
				for(int i=0; i<16; i++) {
						//LOG("char_read [3] =", char_read[i]);
						if(char_read[i] != 0x00) BLEaddress_Changed_Bytes++;
				}
				
				if (BLEaddress_Changed_Bytes) {
						// To Prevent Bad Values (Bad values: When all values are equal)
						uint8_t Equal_Bytes = 0;
						for(int i=0; i<6; i++) {
								uint8_t	local_equal_bytes = 0;
							
								for(int j=0; j<6; j++) {
										if(char_read[i] == char_read[j]) {
												local_equal_bytes++;
										}
								}
								if(local_equal_bytes > Equal_Bytes) {
										Equal_Bytes = local_equal_bytes;
								}
						}
						
						LOG("\nEqual_Bytes\n", Equal_Bytes);
						
						if(Equal_Bytes < 4) {
								char_read[15] = 0x1a;
								uint8_t		operation			=	pstorage_operation_update;
								uint8_t		Destination		=	pstorage_Destination_BLEaddress;
								uint8_t*	InOut_Vector	= char_read;
								pstorage_load_store_clear_update(operation,
																								 Destination,
																								 InOut_Vector	//16 Bytes
								);
						}
						sd_nvic_SystemReset();
				}
				
		}	
		
		if(Counter_Data_Available > Previous_Counter) {
				Previous_Counter = Counter_Data_Available;																									
						
				#ifdef Phase_3
						State_Assigner();
			
						switch(State) {
							case State_Send_OK_plus_Rand:
									Func_Send_OK_plus_Rand();
							break; //State_Send_OK_plus_Rand
									
							case State_Rand_plus_OK_Received:
									if( Func_Check_Rand_plus_OK() ) {

											Func_Send_Payment_Done();
											LOG_String("PaymentDone Received OK");
										
											nrf_gpio_pin_set(LED1_G);					
											nrf_delay_ms(500);
											nrf_gpio_pin_clear(LED1_G);
									}
									else {
											sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);

											// TODO:Put Sender in BlackList
											LOG_String("Checking Rand_plus_OK Failed !");
									}
							break; //State_Rand_plus_OK_Received
									
							case State_Successful_Received:
									if( Func_Check_Successful() ) {
											LOG_String("Successful Received OK");
											sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);													
									} else {
											LOG_String("Checking Successful Failed !");
											sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);																										
									}
							break; //State_Successful_Received
						}
				#endif // Phase_3
				
		}
			
		// read battery voltage and charge state 30 second
		if(read_battery_flag)
		{
			read_battery_flag = false;
											
			nrf_drv_adc_buffer_convert(adc_buffer,ADC_BUFFER_SIZE);
			for (uint16_t i = 0; i < ADC_BUFFER_SIZE; i++)
			{
				// manually trigger ADC conversion
				nrf_drv_adc_sample();
				nrf_delay_us(1000);
			}
			battery_level = ((adc_buffer[0]+adc_buffer[1]+adc_buffer[2]+adc_buffer[3])>>2);
			
			LOG_ADC(battery_level);
			if(battery_level<LOW_BATTERY_LEVEL)  // if battery level under 20%
			{
				strategy_state(&low_battery,true);			
				low_battery_flag=true;
			}else  // if battery level normal
			{
				strategy_state(&low_battery,false);					
				low_battery_flag=false;
			}
		}

		if(button_state)
		{
			button_state=false;
			sd_power_gpregret_clr(0xff);
			sd_power_gpregret_set(0xB1);
			nrf_delay_us(100000);
			NVIC_SystemReset();
		}
	}
}
