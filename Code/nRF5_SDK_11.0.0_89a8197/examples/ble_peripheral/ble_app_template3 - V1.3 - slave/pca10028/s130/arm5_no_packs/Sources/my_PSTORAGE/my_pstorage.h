#ifndef MY_PSTORAGE_H
#define MY_PSTORAGE_H

#include <stdio.h>
#include <stdint.h>
#include "pstorage.h"

#include "bsp.h"
#include "i2c.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "nrf_drv_rng.h"
#include "nrf_assert.h"


#define		pstorage_operation_clear		0
#define		pstorage_operation_store		1
#define		pstorage_operation_load			2
#define		pstorage_operation_update		3

#define		pstorage_Destination_PrivateKey		0
#define		pstorage_Destination_BLEaddress		1


static uint8_t 						pstorage_wait_flag 		= 0;
static pstorage_block_t 	pstorage_wait_handle 	= 0;


void my_Pstorage_Registeration(void);

void pstorage_load_store_clear_update(uint8_t		operation,
																			uint8_t		Destination,
																			uint8_t*	InOut_Vector	//16 Bytes
																			);

#endif /* MY_PSTORAGE_H */
