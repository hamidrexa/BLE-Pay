#ifndef MY_RNG_H
#define MY_RNG_H

#include <stdio.h>
#include <stdint.h>
#include "bsp.h"
#include "nrf_delay.h"
#include "app_error.h"
#include "nrf_drv_rng.h"
#include "nrf_assert.h"


#define RANDOM_BUFF_SIZE 	8                                                           /**< Random numbers buffer size. */

uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size);




#endif /* MY_RNG_H */
