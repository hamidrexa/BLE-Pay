/**
  ******************************************************************************
  * @file    i2c.h
  * @author  Thomas R.
  * @version V1.0
  * @date    15/11/15
  * @brief   header file for i2c driver
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef I2C_H_INCLUDED
#define I2C_H_INCLUDED

/* Includes ------------------------------------------------------------------*/
#include <string.h>
#include "nrf_drv_twi.h"
#include "nrf_delay.h"
#include "app_util_platform.h"
#include "nrf_soc.h"
#include "app_trace.h"
#include "SEGGER_RTT.h"

/* Exported types ------------------------------------------------------------*/
// PINs:
		// PINs:
		#if defined PCB1
				#define	SCL_pin		28
				#define	SDA_pin		29
		#endif

// HARD CODE:
static inline void LOG(char* TAG, int line){
				char buf[100];
				sprintf(buf, "%s %x\n", TAG, line);	
				SEGGER_RTT_WriteString(0, buf);	
}

static inline void LOG_Payment(char* TAG, uint8_t up, uint8_t down) {
				char buf[100];
				sprintf(buf, "%s %02x%02x\n", TAG, up, down);	
				SEGGER_RTT_WriteString(0, buf);
}

static inline void LOG_uint8_t(int line){
				char buf[100];
				sprintf(buf, "%x\n", line);	
				SEGGER_RTT_WriteString(0, buf);	
}

static inline void LOG_String(char* TAG){
				char buf[100];
				sprintf(buf, "%s\n", TAG);	
				SEGGER_RTT_WriteString(0, buf);	
}

static inline void LOG_String_and_uint8_t(char* TAG, int line){
				char buf[100];
				sprintf(buf, "%s%x\n", TAG, line);	
				SEGGER_RTT_WriteString(0, buf);	
}

static inline void LOG_n(void){
				char buf[100];
				sprintf(buf, "\n");	
				SEGGER_RTT_WriteString(0, buf);	
}

// HARD CODE:
// ADC
//#ifdef ADC 
static inline void LOG_ADC(int16_t adc_value){
				char buf[100];
				char* ADC_1 	= "ADC_1 =";
					
				sprintf(buf, "ADC:  %s %4d \n", ADC_1, adc_value);	
				SEGGER_RTT_WriteString(0, buf);	
}
//#endif

/* Exported constants --------------------------------------------------------*/
#define I2CERRORCOUNT_TO_RECOVER    5
#define I2CBUFFERSIZE               20

/* Exported macros -----------------------------------------------------------*/

/* Exported functions ------------------------------------------------------- */
uint32_t i2c_Init(void);
void i2c_Deinit(void);
uint32_t i2c_read(uint8_t device_address, uint8_t register_address, uint8_t length, uint8_t *data);
uint32_t i2c_write(uint8_t device_address, uint8_t register_address, uint8_t length, uint8_t *data);

#endif /* I2C_H_INCLUDED */

/**END OF FILE*****************************************************************/
