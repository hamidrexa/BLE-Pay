/*
****************************************************************************
* Copyright (C) 2017 HARD
*
* bme280_support.c
* Date: 2017/09/03
* Revision: 1.0.0 $
*
* Usage: pstorage Driver support file for Nrf51822
*
****************************************************************************/
// HARD CODE:
// PSTORAGE

#include "my_pstorage.h"

pstorage_handle_t		block_0_handle; // PrivateKey
pstorage_handle_t		block_1_handle; // BLEaddress


void	Indicate_By_GPIO(uint8_t	pin, uint32_t	delay) {
		nrf_gpio_pin_set(pin);
		nrf_delay_ms(delay);
		nrf_gpio_pin_clear(pin);
}


void pstorage_callback_handler(pstorage_handle_t  * handle,
															 uint8_t              op_code,
                               uint32_t             result,
                               uint8_t            * p_data,
                               uint32_t             data_len)
{
		if(handle->block_id == pstorage_wait_handle) { pstorage_wait_flag = 0; }  //If we are waiting for this callback, clear the wait flag.
	
		switch(op_code)
		{
			case PSTORAGE_LOAD_OP_CODE:
				 if (result == NRF_SUCCESS)
				 {
						 LOG_String("pstorage LOAD callback received");
				 }
				 else
				 {
						 LOG_String("pstorage LOAD ERROR callback received");
				 }
				 break;
			case PSTORAGE_STORE_OP_CODE:
				 if (result == NRF_SUCCESS)
				 {
						 LOG_String("pstorage STORE callback received");
				 }
				 else
				 {
					   LOG_String("pstorage STORE ERROR callback received");
				 }
				 break;				 
			case PSTORAGE_UPDATE_OP_CODE:
				 if (result == NRF_SUCCESS)
				 {
						 LOG_String("pstorage UPDATE callback received");
				 }
				 else
				 {
						 LOG_String("pstorage UPDATE ERROR callback received");
				 }
				 break;
			case PSTORAGE_CLEAR_OP_CODE:
				 if (result == NRF_SUCCESS)
				 {
						 LOG_String("pstorage CLEAR callback received");
				 }
				 else
				 {
					   LOG_String("pstorage CLEAR ERROR callback received");
				 }
				 break;	 
		}			
}


void my_Pstorage_Registeration(void)
{
		pstorage_handle_t				handle;
		pstorage_module_param_t param;
		uint32_t								ret;
	
		param.block_size  = 16;                  				//Select block size of 16 bytes
		param.block_count = 2;                    			//Select 2 blocks, total of 32 bytes
		param.cb          = pstorage_callback_handler;  //Set the pstorage callback handler
			
		ret = pstorage_register(&param, &handle);
		if(ret != NRF_SUCCESS)	{
				Indicate_By_GPIO(18, 500);
				LOG_String("Pstorage_Register Failed !");
		}
		
		//Get block identifiers
		pstorage_block_identifier_get(&handle, 0, &block_0_handle);
		pstorage_block_identifier_get(&handle, 1, &block_1_handle);	
}

void pstorage_load_store_clear_update(uint8_t		operation,
																			uint8_t		Destination,
																			uint8_t*	InOut_Vector	//16 Bytes
																			)
{
		uint32_t                ret;
		
//		//Clear
//		//Clear 32 bytes, i.e. 2 blocks because param.block_size is set to 16
//		LOG_String("pstorage clear 32 bytes from block 0 \r\n");
//		pstorage_wait_handle = block_0_handle.block_id;            				//Specify which pstorage handle to wait for
//		pstorage_wait_flag = 1;                                    				//Set the wait flag. Cleared in the pstorage_callback_handler
//		pstorage_clear(&block_0_handle, 2 * 16);                   				//Clear 32 bytes
//		LOG_String("pstorage wait for clear to complete ... \r\n");
//		while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); } //Sleep until clear operation is finished.
		
//		//Load
//		LOG_String("pstorage load blocks 0 \r\n");
//		pstorage_load(dest_data_0, &block_0_handle, 16, 0);				 //Read from flash, only one block is allowed for each pstorage_load command
//		LOG_String("pstorage load blocks 1 \r\n");
//		pstorage_load(dest_data_1, &block_1_handle, 16, 0);				 //Read from flash

//		//Store
//		LOG_String("pstorage store into block 0 \r\n");
//		pstorage_wait_handle = block_0_handle.block_id;            					//Specify which pstorage handle to wait for
//		pstorage_wait_flag = 1;                                    					//Set the wait flag. Cleared in the pstorage_callback_handler
//		pstorage_store(&block_0_handle, Input_Vector, 16, 0);     					//Write to flash
//		LOG_String("pstorage wait for block 0 store to complete ... \r\n");
//		while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }   //Sleep until store operation is finished.

//		//Update
//		LOG_String("pstorage update block 0 \r\n");
//		pstorage_wait_handle = block_0_handle.block_id;            //Specify which pstorage handle to wait for 
//		pstorage_wait_flag = 1;                                    //Set the wait flag. Cleared in the pstorage_callback_handler
//		pstorage_update(&block_0_handle, source_data_9, 16, 0);    //update flash block 0
//		LOG_String("pstorage wait for update to complete ... \r\n");
//		while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }              //Sleep until update operation is finished.
		
		switch(operation) {
				case pstorage_operation_clear:
					
						switch(Destination) {
								case pstorage_Destination_PrivateKey:
										LOG_String("pstorage clear 16 bytes from PrivateKey");
										pstorage_wait_handle = block_0_handle.block_id;            				//Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                                    				//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for clear PrivateKey to complete ...");
										pstorage_clear(&block_0_handle, 1 * 16);                   				//Clear 16 bytes
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); } //Sleep until clear operation is finished.
								break;
								case pstorage_Destination_BLEaddress:
										LOG_String("pstorage clear 16 bytes from BLEaddress");
										pstorage_wait_handle = block_1_handle.block_id;            				//Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                                    				//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for clear BLEaddress to complete ...");
										pstorage_clear(&block_1_handle, 1 * 16);                   				//Clear 16 bytes
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); } //Sleep until clear operation is finished.
								break;
						}
						
						
				break; //pstorage_operation_clear
				
				case pstorage_operation_store:
					
						switch(Destination) {
								case pstorage_Destination_PrivateKey:
										LOG_String("pstorage store into PrivateKey");
										pstorage_wait_handle = block_0_handle.block_id;            					//Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                                    					//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for PrivateKey store to complete ...");
										pstorage_store(&block_0_handle, InOut_Vector, 16, 0);     					//Write to flash
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }   //Sleep until store operation is finished.									
								break;
								case pstorage_Destination_BLEaddress:
										LOG_String("pstorage store into BLEaddress");
										pstorage_wait_handle = block_1_handle.block_id;            					//Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                                    					//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for BLEaddress store to complete ...");
										pstorage_store(&block_1_handle, InOut_Vector, 16, 0);     						//Write to flash
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }   //Sleep until store operation is finished.																		
								break;
						}
						
				break; //pstorage_operation_store
				
				case pstorage_operation_load:

						switch(Destination) {
								case pstorage_Destination_PrivateKey:
										LOG_String("pstorage load PrivateKey");
										pstorage_wait_handle = block_0_handle.block_id;       //Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                               //Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for PrivateKey load to complete ...");
										pstorage_load(InOut_Vector, &block_0_handle, 16, 0);	//Read from flash, only one block is allowed for each pstorage_load command
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }   //Sleep until store operation is finished.																		
								break;
								case pstorage_Destination_BLEaddress:
										LOG_String("pstorage load BLEaddress");
										pstorage_wait_handle = block_1_handle.block_id;       //Specify which pstorage handle to wait for
										pstorage_wait_flag = 1;                               //Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for BLEaddress load to complete ...");
										pstorage_load(InOut_Vector, &block_1_handle, 16, 0);	//Read from flash
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); }   //Sleep until store operation is finished.																		
								break;
						}
						
				break; //pstorage_operation_load
				
				case pstorage_operation_update:

						switch(Destination) {
								case pstorage_Destination_PrivateKey:
										LOG_String("pstorage update PrivateKey");
										pstorage_wait_handle = block_0_handle.block_id;            				//Specify which pstorage handle to wait for 
										pstorage_wait_flag = 1;                                    				//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for update PrivateKey to complete ...");
										pstorage_update(&block_0_handle, InOut_Vector, 16, 0);    				//update flash block 0
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); } //Sleep until update operation is finished.
										LOG_String("update PrivateKey completed!");
								break;
								case pstorage_Destination_BLEaddress:
										LOG_String("pstorage update BLEaddress");
										pstorage_wait_handle = block_1_handle.block_id;            				//Specify which pstorage handle to wait for 
										pstorage_wait_flag = 1;                                    				//Set the wait flag. Cleared in the pstorage_callback_handler
										LOG_String("pstorage wait for update BLEaddress to complete ...");
										pstorage_update(&block_1_handle, InOut_Vector, 16, 0);    				//update flash block 0
										while(pstorage_wait_flag) { APP_ERROR_CHECK(sd_app_evt_wait()); } //Sleep until update operation is finished.
										LOG_String("update BLEaddress completed!");
								break;
						}
				
							
				break; //pstorage_operation_update
		}

		
}





