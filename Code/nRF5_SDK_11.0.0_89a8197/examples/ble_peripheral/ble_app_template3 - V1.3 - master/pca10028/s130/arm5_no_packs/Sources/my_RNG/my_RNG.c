/*
****************************************************************************
* Copyright (C) 2017 HARD
*
* bme280_support.c
* Date: 2017/09/03
* Revision: 1.0.0 $
*
* Usage: RNG Driver support file for Nrf51822
*
****************************************************************************/
// HARD CODE:
// RNG

#include "my_RNG.h"

/** @brief Function for getting vector of random numbers.
 *
 * @param[out] p_buff                               Pointer to unit8_t buffer for storing the bytes.
 * @param[in]  length                               Number of bytes to take from pool and place in p_buff.
 *
 * @retval     Number of bytes actually placed in p_buff.
 */
uint8_t random_vector_generate(uint8_t * p_buff, uint8_t size)
{
    uint8_t available;
    uint32_t err_code;
    err_code = nrf_drv_rng_bytes_available(&available);
    APP_ERROR_CHECK(err_code);
    uint8_t length = (size<available) ? size : available;
    err_code = nrf_drv_rng_rand(p_buff,length);
    APP_ERROR_CHECK(err_code);
    return length;
}

